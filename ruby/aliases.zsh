alias r='rvm use 1.8.7'

alias sc='script/console'
alias sg='script/generate'
alias sd='script/destroy'

alias migrate='be rake db:migrate db:test:clone'

alias be='nocorrect noglob bundle exec'
alias budo='be'
alias rake='noglob rake'

alias ss='script/specs'
alias sa='script/acceptance'

alias rk='rake'
